var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var Sequelize = require("sequelize");
var session = require('express-session')
var expressValidator = require('express-validator');
var util = require('util');

var sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'sqlite',
    storage: './database.sqlite'
});
sequelize
    .authenticate()
    .then(function(err) {
        console.log('Connection has been established successfully.');
    })
    .catch(function(err) {
        console.log('Unable to connect to the database:', err);
    });

var Board = sequelize.define('board', {
    subject: {
        type: Sequelize.STRING
    },
    contents: {
        type: Sequelize.STRING
    },
    username: {
        type: Sequelize.STRING
    }
});
var User = sequelize.define('user', {
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    }
});

sequelize.sync({});

app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(expressValidator([]));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}))


app.use('/', express.static('static'));

app.get('/', function(req, res) {
    res.render('app', {
        session: req.session.email !== 'undefined' ? req.session.email : ''
    });
});
app.get('/login', function(req, res) {
    res.render('login', {});
});
app.get('/join', function(req, res) {
    res.render('user', {});
});
app.get('/list', function(req, res) {
    res.render('app', {
        session: req.session.email !== 'undefined' ? req.session.email : ''
    });
});
app.get('/add', function(req, res) {
    console.log(req.session);
    res.render('add', {
        username: req.session.username
    });
});
app.get('/view', function(req, res) {
    res.render('view', {
        username: req.session.username
    });
});
app.get('/edit', function(req, res) {
    res.render('edit', {
        username: req.session.username
    });
});

app.get('/article', function(req, res) {
    Board.findAll({
        order: 'id DESC'
    }).then(function(board) {
        var _board = JSON.stringify(board);
        return res.send({
            'list': JSON.parse(_board)
        });
    });
});
app.get('/article/:id', function(req, res) {
    console.log(req.params.id);
    Board.find({
        where: {
            id: req.params.id
        }
    }).then(function(ret) {
        // console.log(ret)
        var article = JSON.stringify(ret);
        console.log(article)
        return res.send({
            'view': JSON.parse(article)
        });
    })
});

app.post('/article', function(req, res) {
    //@TODO 로그인한 유저가 요청한건지 확인하기
    Board.create({
        subject: req.body.subject,
        contents: req.body.contents,
        username: req.body.username
    }).then(function() {
        return res.send({
            result: true
        });
    });

});
app.put('/article', function(req, res) {
    //@TODO 로그인한 유저가 요청한건지 확인하기
    Board.update({
        subject: req.body.subject,
        contents: req.body.contents
    }, {
        where: {
            id: req.body.id
        }
    }).then(function(aa) {
        // console.log(aa)
        return res.send({
            'result': true
        });
    });
});
app.delete('/article/:id', function(req, res) {
    //@TODO 로그인한 유저가 요청한건지 확인하기
    if (req.params.id == '' || typeof req.params.id == 'undefined' || req.params.id == null) {
        return res.send({
            result: false
        });
    }
    Board.destroy({
        where: {
            id: req.params.id
        }
    }).then(function() {
        return res.send({
            'result': true
        });
    });
});
app.post('/users', function(req, res) {
    if (req.body.name == '' || typeof req.body.name == 'undefined' || req.body.name == null) {
        return res.send({
            result: false
        });
    }
    if (req.body.email == '' || typeof req.body.email == 'undefined' || req.body.email == null) {
        return res.send({
            result: false
        });
    }
    if (req.body.password == '' || typeof req.body.password == 'undefined' || req.body.password == null) {
        return res.send({
            result: false
        });
    }
    User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }).then(function() {
        return res.send({
            result: true
        });
    });
});
app.post('/login', function(req, res) {
    req.checkBody({
        'email': {
            notEmpty: true,
            isEmail: {
                errorMessage: 'Invalid Email'
            }
        },
        'password': {
            notEmpty: true,
            errorMessage: 'Invalid Password'
        }
    });
    var errors = req.validationErrors();
    if (errors) {
        return res.send({
            result: false,
            msg: 'There have been validation errors: ' + util.inspect(errors)
        });
    }
    User.findOne({
        where: {
            email: req.body.email,
            password: req.body.password
        }
    }).then(function(ret) {
        var user = JSON.stringify(ret);
        if (user === 'null') {
            return res.send({
                result: false,
                msg: 'Invalid User or not match password'
            });
        }
        user = JSON.parse(user);
        req.session.email = req.body.email;
        req.session.userid = user.id;
        req.session.username = user.name;
        res.cookie('username', req.session.username);
        return res.send({
            result: true
        });
    });
});
app.get('/logout', function(req, res) {
    req.session.destroy(function(err) {
        if (err !== undefined) {
            return res.send({
                result: false
            });
        }
        return res.send({
            result: true
        });
    })
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});
